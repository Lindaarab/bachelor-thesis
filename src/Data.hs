module Data where

type Matrix a = [Row a]
type Row a = [a]

type Grid = Matrix Square
data Square = Square { color :: Color, value :: Value } deriving Eq
data Color = White | Black deriving Eq
type Value = Int

grid :: Grid
grid = [
    [ Square White 3, Square White 0, Square Black 0, Square Black 6, Square White 0, Square White 0 ],
    [ Square White 0, Square White 0, Square White 0, Square White 0, Square White 0, Square Black 1 ],
    [ Square Black 0, Square White 0, Square White 0, Square White 0, Square White 0, Square White 0 ],
    [ Square Black 0, Square White 0, Square White 0, Square White 0, Square White 0, Square White 0 ],
    [ Square White 2, Square White 0, Square Black 3, Square White 0, Square White 0, Square Black 0 ],
    [ Square Black 0, Square Black 0, Square White 0, Square White 5, Square White 0, Square Black 0]
    ]
