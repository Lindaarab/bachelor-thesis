module Str8ts where

import Data ( Color(White, Black), Square(..), Grid, Matrix )
import Prelude hiding (splitAt)

--

size :: Int
size = 6

--

white :: Square -> Bool
white s = color s == White

black :: Square -> Bool
black s = color s == Black

empty :: Square -> Bool
empty s = value s == 0

squares :: [Square]
squares = [Square White v | v <- [1..size]]

--

solve :: Grid -> [Grid]
solve = filter valid . expand . choices

choices :: Grid -> Matrix Choices
expand :: Matrix Choices -> [Grid]
valid :: Grid -> Bool

--

type Choices = [Square]

choices = map (map choice)

choice :: Square -> Choices
choice s = if white s && empty s then squares else [s]

--

expand = cp . map cp

cp :: [[a]] -> [[a]]
cp [] = [[]]
cp (xs:xss) = [x:ys | x <- xs, ys <- cp xss]

--

valid g = all validRow (rows g) && 
          all validRow (columns g)

validRow :: [Square] -> Bool
validRow ss = condition1 ss && condition2 ss

condition1 :: [Square] -> Bool
condition1 ss = noDuplicates [value s | s <- ss, not (black s && empty s)]

condition2 :: [Square] -> Bool
condition2 = all (noGaps . map value) . compartments

--

noDuplicates :: Eq a => [a] -> Bool
noDuplicates [] = True
noDuplicates (x:xs) = notElem x xs && noDuplicates xs

--

noGaps :: [Int] -> Bool
noGaps xs = maximum xs - minimum xs < length xs

--

compartments :: [Square] -> [[Square]]
compartments = splitAt black

splitAt :: (a -> Bool) -> [a] -> [[a]]
splitAt f xs = case dropWhile f xs of
    [] -> []
    xs' -> x : splitAt f xs''
        where (x,xs'') = break f xs'

--

rows :: Matrix a -> Matrix a
rows = id

columns :: Matrix a -> Matrix a
columns [xs] = [[x] | x <- xs]
columns (xs:xss) = zipWith (:) xs (columns xss)

--
